### O:

- Today we learned the basics of react-router, using axios in React, and the fundamentals of React hooks.
- React Router provides three different ways to implement page navigation and route management: BrowserRouter, HashRouter, and MemoryRouter. They facilitate better management of single-page web applications.
- Axios is a modern HTTP client library based on Promise. It offers a simple and intuitive way to execute asynchronous network requests, making it easier for us to handle network communication. It supports Promise, allows request and response interception and transformation, and provides convenient error handling.
- Ant Design is an enterprise-level UI component library based on React. It offers rich components, responsive design, and high customizability, making our development process more convenient.

### R:

- Today's overall progress is smooth.

### I:

- I am not yet familiar enough with Ant Design, which has led to relatively lower efficiency in its usage.
- Dealing with asynchronous operations in Redux can be cumbersome.

### D:

- Keep my enthusiasm for learning