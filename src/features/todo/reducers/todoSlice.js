import {createSlice} from '@reduxjs/toolkit'

export const todoSlice = createSlice({
  name: 'todo',
  initialState: {
    tasks: []
  },
  reducers: {
    initialTasks: (state, action) => {
      state.tasks = action.payload;
    }
  }
})

export const {removeTodoTask, initialTasks} = todoSlice.actions
export default todoSlice.reducer