import TodoItem from "./TodoItem";
import {useSelector} from 'react-redux'
import {Divider, List} from "antd";

export default function TodoGroup() {
  const todoTasks = useSelector(state => state.todo.tasks)
  return (
    <div>
      <List
        bordered
        dataSource={todoTasks}
        renderItem={(todoTask) => (
          <List.Item>
            <TodoItem key={todoTask.id} task={todoTask}></TodoItem>
          </List.Item>
        )}
      />
    </div>
  );
}