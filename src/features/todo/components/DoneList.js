import DoneGroup from "./DoneGroup";

export default function DoneList() {
  return (
    <div>
      <h1 style={{textAlign: 'center'}}>Done List</h1>
      <DoneGroup/>
    </div>
  );
}