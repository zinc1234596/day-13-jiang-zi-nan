import {useSelector} from 'react-redux'
import DoneItem from "./DoneItem";
import {List} from "antd";

export default function DoneGroup() {
  const doneTasks = useSelector(state => state.todo.tasks).filter(todo => todo.done === true)
  return (
    <div>
      <List
        bordered
        dataSource={doneTasks}
        renderItem={(doneTask) => (
          <List.Item>
            <DoneItem key={doneTask.id} task={doneTask}></DoneItem>
          </List.Item>
        )}
      />
    </div>
  );
}