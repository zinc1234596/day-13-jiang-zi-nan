import {Button, Input, Modal, Popover} from "antd";
import {useTodos} from "../../../hooks/useTodo";
import {useState} from "react";

export default function DoneItem(props) {
  const {id, name, done} = props.task;
  const {deleteTodo, updateTodo} = useTodos();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [editText, setEditText] = useState(name);
  const confirmDelete = () => {
    Modal.confirm({
      title: 'Confirm',
      content: 'Are you sure you wish to delete this item?',
      okText: 'Delete',
      cancelText: 'Cancel',
      onOk: async () => {
        await deleteTodo(id);
      }
    });
  };
  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = async () => {
    await updateTodo(id, {name: editText});
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setEditText(name);
  };
  return (
    <div style={{width: '100%'}}>
      <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
        <div style={{
          textDecoration: done ? 'line-through' : 'none',
          cursor: 'pointer',
          wordBreak: 'break-word',
          width: '65%'
        }}>
          {name}
        </div>
        <div>
          <Popover content={name} trigger="click" >
            <Button size="small" type="primary" style={{marginRight: '10px',background: 'green'}}>Detail</Button>
          </Popover>
          <Button size="small" type="primary" onClick={showModal} style={{marginRight: '10px'}}>
            Edit
          </Button>
          <Button size="small" type="primary" danger onClick={confirmDelete}>
            Delete
          </Button>
        </div>

      </div>
      <Modal
        title="Edit Todo"
        open={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <Input
          value={editText}
          onChange={(e) => setEditText(e.target.value)}
        />
      </Modal>
    </div>
  );
}