import TodoGenerator from "./TodoGenerator";
import TodoGroup from "./TodoGroup";
import {useEffect} from "react";
import {useTodos} from "../../../hooks/useTodo";

export default function TodoList() {
  const {loadTodos} = useTodos();
  useEffect(() => {
    loadTodos().then()
  }, [loadTodos]);
  return (
    <div>
      <h1 style={{textAlign: 'center'}}>Todo List</h1>
      <TodoGenerator/>
      <TodoGroup/>
    </div>
  );
}