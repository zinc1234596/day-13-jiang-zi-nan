import {useState} from "react";
import {useTodos} from "../../../hooks/useTodo";
import {Button, Input, Modal, Popover} from "antd";

export default function TodoItem(props) {
  const {id, name, done} = props.task;
  const {updateTodo, deleteTodo,getTodo} = useTodos();
  const handleTaskNameClick = async () => {
    await updateTodo(id, {done: !done});
  };

  const [editText, setEditText] = useState(name);
  const [detailText, setDetailText] = useState(name);
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleDetail = async () =>{
    const todo = await getTodo(id)
    setDetailText(todo.name)
  }

  const handleOk = async () => {
    await updateTodo(id, {name: editText});
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setEditText(name);
  };

  const confirmDelete = () => {
    Modal.confirm({
      title: 'Confirm',
      content: 'Are you sure you wish to delete this item?',
      okText: 'Delete',
      cancelText: 'Cancel',
      onOk: async () => {
        await deleteTodo(id);
      }
    });
  };
  return (
    <div style={{width: '100%'}}>
      <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
        <div style={{
          textDecoration: done ? 'line-through' : 'none',
          cursor: 'pointer',
          wordBreak: 'break-word',
          width: '65%'
        }}
             onClick={handleTaskNameClick}>
          {name}
        </div>
        <div>
          <Popover content={detailText} trigger="click" >
            <Button size="small" type="primary" style={{marginRight: '10px',background: 'green'}} onClick={handleDetail}>Detail</Button>
          </Popover>
          <Button size="small" type="primary" onClick={showModal} style={{marginRight: '10px'}}>
            Edit
          </Button>
          <Button size="small" type="primary" danger onClick={confirmDelete}>
            Delete
          </Button>
        </div>
      </div>
      <Modal
        title="Edit Todo"
        open={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <Input
          value={editText}
          onChange={(e) => setEditText(e.target.value)}
        />
      </Modal>
    </div>
  );
}
