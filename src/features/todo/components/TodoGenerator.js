import {useState} from "react";
import {useTodos} from "../../../hooks/useTodo";
import {Button, Input} from "antd";

export default function TodoGenerator() {
  const [taskName, setTaskName] = useState("");
  const {addTodo} = useTodos();

  const handleTaskNameChange = (e) => {
    const value = e.target.value;
    setTaskName(value);
  };

  const handleKeyDown = async (e) => {
    if (taskName && e.key === "Enter") {
      await handleAddTodoTask();
    }
  };

  const handleAddTodoTask = async () => {
    await addTodo({name: taskName});
    setTaskName("");
  };

  return (
    <div style={{display: 'flex', margin: '10px 0', height: '20px', alignItems: 'center'}}>
      <Input placeholder="input a new todo here..." onChange={handleTaskNameChange}
             onKeyDown={handleKeyDown}
             value={taskName}/>
      <Button type="primary" shape="default" onClick={handleAddTodoTask} disabled={!taskName}>Add Todo</Button>
    </div>
  );
}
