import './App.css';
import {Outlet, useNavigate} from "react-router-dom";
import {Layout, Menu} from "antd";
import {Content, Header} from "antd/es/layout/layout";
import {useState} from "react";

const items = [
  {
    label: 'Home',
    key: '1',
    path: '/'
  },
  {
    label: 'Done List',
    key: '2',
    path: '/done'
  },
  {
    label: 'Help',
    key: '3',
    path: '/help'
  },
]

function App() {
  const navigate = useNavigate();
  const [current, setCurrent] = useState('1');
  const onClick = (e) => {
    setCurrent(e.key);
    const item = items.find(i => i.key === e.key);
    navigate(item.path);
  };
  return (
    <div className="app">
      <Layout>
        <Header style={{padding: 0}}>
          <Menu onClick={onClick} selectedKeys={[current]} theme="dark" mode="horizontal" items={items}/>;
        </Header>
        <Content>
          <Outlet></Outlet>
        </Content>
      </Layout>
    </div>
  );
}

export default App;
