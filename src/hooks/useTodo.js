import * as todoApi from "../apis/todo";
import {initialTasks} from "../features/todo/reducers/todoSlice";
import {useDispatch} from "react-redux";

export const useTodos = () => {
  const dispatch = useDispatch();
  const loadTodos = async () => {
    const res = await todoApi.getTodoTasks();
    dispatch(initialTasks(res));
  }

  const updateTodo = async (id, todoTask) => {
    await todoApi.updateTodoTask(id, todoTask);
    await loadTodos()
  }

  const deleteTodo = async (id) => {
    await todoApi.deleteTodoTask(id);
    await loadTodos()
  }

  const addTodo = async (todoTask) => {
    await todoApi.addTodoTask(todoTask)
    await loadTodos()
  }

  const getTodo = async (id) => {
    return await todoApi.getTodoTaskById(id)
  }

  return {loadTodos, updateTodo, deleteTodo, addTodo,getTodo};
}