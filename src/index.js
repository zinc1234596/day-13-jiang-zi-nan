import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import store from './store'
import {Provider} from 'react-redux'
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import staticRouter from "./router";

const root = ReactDOM.createRoot(document.getElementById('root'));
const router = createBrowserRouter([...staticRouter]);
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router}>
      </RouterProvider>
    </Provider>
  </React.StrictMode>
);
