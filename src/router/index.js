import App from "../App";
import ErrorPage from "../pages/ErrorPage";
import HelpPage from "../pages/HelpPage";
import DoneList from "../features/todo/components/DoneList";
import TodoList from "../features/todo/components/TodoList";
import DoneDetail from "../features/todo/components/DoneDetail";

const staticRouter = [
  {
    path: "/",
    element: <App/>,
    errorElement: <ErrorPage/>,
    children: [
      {
        index: true,
        element: <TodoList/>
      },
      {
        path: "/help",
        element: <HelpPage/>
      },
      {
        path: "/done",
        element: <DoneList/>
      },
      {
        path: "/done/:id",
        element: <DoneDetail/>
      }
    ]
  },
];

export default staticRouter;