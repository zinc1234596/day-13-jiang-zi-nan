import {del, get, post, put} from "./index";

export const getTodoTasks = async () => {
  return await get('/todo');
}

export const updateTodoTask = async (id, todoTask) => {
  return await put(`/todo/${id}`, todoTask);
}

export const addTodoTask = async (todoTask) => {
  return await post('/todo', todoTask);
}

export const deleteTodoTask = async (id) => {
  return await del(`/todo/${id}`);
}

export const getTodoTaskById = async (id) => {
  return await get(`/todo/${id}`);
}