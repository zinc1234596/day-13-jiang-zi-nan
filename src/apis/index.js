import axios from 'axios';

const api = axios.create({
  // baseURL: 'https://64c0dafafa35860bae9f848f.mockapi.io/api/v1/',
  baseURL: 'http://localhost:7890',
  timeout: 5000,
});

api.interceptors.request.use(
  (config) => {
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);
api.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export const get = async (url, params) => {
  try {
    const response = await api.get(url, {params});
    return response.data;
  } catch (error) {
    throw new Error(error.response.data.error);
  }
};

export const post = async (url, data) => {
  try {
    const response = await api.post(url, data);
    return response.data;
  } catch (error) {
    throw new Error(error.response.data.error);
  }
};

export const put = async (url, data) => {
  try {
    const response = await api.put(url, data);
    return response.data;
  } catch (error) {
    throw new Error(error.response.data.error);
  }
};

export const del = async (url) => {
  try {
    const response = await api.delete(url);
    return response.data;
  } catch (error) {
    throw new Error(error.response.data.error);
  }
};
